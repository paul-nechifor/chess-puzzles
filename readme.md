# Chess Puzzles

Chess puzzles web app.

![Chess puzzles screenshot.](screenshot.png)

## Usage

Start the server and watch for changes:

    npm start

## License

ISC
